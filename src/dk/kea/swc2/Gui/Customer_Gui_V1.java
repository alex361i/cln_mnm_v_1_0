/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.Gui;

import dk.kea.swc2.dataClasses.ConnectToMySQL;
import dk.kea.swc2.dataClasses.Customer;
import dk.kea.swc2.dataClasses.Employee;
import dk.kea.swc2.dataClasses.Address;
import dk.kea.swc2.dataClasses.DBConnector;
import dk.kea.swc2.dataClasses.Crud_for_customer;
import dk.kea.swc2.dataClasses.Crud_for_employee;
import dk.kea.swc2.dataClasses.Crud_for_schedule;
import dk.kea.swc2.dataClasses.Display_Schedule;
import dk.kea.swc2.dataClasses.Schedule;
import java.awt.event.ItemEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.ListModel;
import javax.swing.UIManager;

/**
 *
 * @author yo
 */
public class Customer_Gui_V1 extends javax.swing.JFrame
{

    ArrayList<Employee> employeeList = new ArrayList<>();
    ArrayList<Customer> customerList = new ArrayList<>();
    ArrayList<Address> addressList = new ArrayList<>();
    ArrayList<Schedule> scheduleList = new ArrayList<>();
    ArrayList<Schedule> displayList = new ArrayList<>();

    Crud_for_customer crudC = new Crud_for_customer();
    Crud_for_employee crudE = new Crud_for_employee();
    Crud_for_schedule crudS = new Crud_for_schedule();

    int ok = 0;
    int k = 0;
    static int eId = 1;
    static int cId = 1;
    static int idAddress = 1;
    static int idDate = 1;

    public Customer_Gui_V1()
    {

        try
        {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e)
        {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }

        initComponents();
        readData();
        try
        {
            buildComboBoxModelCust();
            buildComboBoxModel();

            buildComboBoxModelReportselectEmp();
            buildComboBoxModelReportselectCust();
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private ListModel<Employee> getListFromAlist(List<Employee> aEmp)
    {
        DefaultListModel<Employee> result = new DefaultListModel<>();

        aEmp.stream().forEach((imp) ->
        {
            result.addElement(imp);
        });
        return result;
    }

    private ListModel<Customer> getListFromlist(List<Customer> aCust)
    {
        DefaultListModel<Customer> cResult = new DefaultListModel<>();

        aCust.stream().forEach((cust) ->
        {
            cResult.addElement(cust);
        });
        return cResult;
    }

    private ListModel<Schedule> getListFromSlist(List<Schedule> aSch)
    {
        DefaultListModel<Schedule> sresult = new DefaultListModel<>();

        aSch.stream().forEach((sch) ->
        {
            sresult.addElement(sch);
        });
        return sresult;
    }

    private ListModel<Schedule> getListFromSlistDisplay(List<Schedule> aSch)
    {
        DefaultListModel<Schedule> sresultDisplay = new DefaultListModel<>();

        aSch.stream().forEach((sch) ->
        {
            sresultDisplay.addElement(sch);
        });
        return sresultDisplay;
    }

    private ListModel<Address> getListFromAlistDisplay(List<Address> aAdr)
    {
        DefaultListModel<Address> aresultDisplay = new DefaultListModel<>();

        aAdr.stream().forEach((adr) ->
        {
            aresultDisplay.addElement(adr);
        });
        return aresultDisplay;
    }

    public void select() throws SQLException, ClassNotFoundException
    {

        Connection conn = DBConnector.getConnection();

        String sqlRead = "SELECT * FROM employee;";

        Statement stmt = conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs = stmt.getResultSet();

        while (rs.next())
        {

            Employee emp = new Employee();
            emp.setId(rs.getString("id"));
            emp.setCnp(rs.getString("cnp"));
            emp.seteName(rs.getString("eName"));
            emp.seteAddress(rs.getString("eAddress"));
            emp.setePhone(rs.getString("ePhone"));
            emp.seteEmail(rs.getString("eEmail"));
            employeeList.add(emp);

            eId = Integer.parseInt(rs.getString("id"));
            eId = eId + 1;

            empIdTfield.setText(eId + "");

            empJList.setModel(getListFromAlist(employeeList));
        }

    }

    public void cSelect() throws SQLException, ClassNotFoundException
    {

        Connection conn = DBConnector.getConnection();

        String sqlRead = "SELECT * FROM customer;";

        Statement stmt = conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs = stmt.getResultSet();

        while (rs.next())
        {

            Customer cust = new Customer();
            cust.setcId(rs.getString("cId"));
            cust.setCvr(rs.getString("cvr"));
            cust.setcName(rs.getString("cName"));
            cust.setcPhone(rs.getString("cPhone"));
            cust.setcEmail(rs.getString("cEmail"));

            customerList.add(cust);
            cId = Integer.parseInt(rs.getString("cId"));
            cId = cId + 1;

            cIdTextField.setText(cId + "");

            customerJList.setModel(getListFromlist(customerList));
            addressJList.setModel(getListFromAlistDisplay(addressList));
        }

    }

    public void cAddresSelect() throws SQLException, ClassNotFoundException
    {

        Connection conn = DBConnector.getConnection();

        String sqlRead = "SELECT * FROM caddress;";

        Statement stmt = conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs = stmt.getResultSet();

        while (rs.next())
        {
            Address cAddress = new Address();
            cAddress = new Address();
            cAddress.setIdCAddress(rs.getString("idCAddress"));
            cAddress.setcAddress1(rs.getString("address1"));
            cAddress.setcAddress2(rs.getString("address2"));
            cAddress.setcAddress3(rs.getString("address3"));
            cAddress.setcAddress4(rs.getString("address4"));
            addressList.add(cAddress);
            cIdAddressTxF.setText(rs.getString("idCAddress"));

        }

    }

    public void sSelect() throws SQLException, ClassNotFoundException
    {

        Connection conn = DBConnector.getConnection();

        String sqlRead = "SELECT * FROM date;";

        Statement stmt = conn.createStatement();

        stmt.execute(sqlRead);
        ResultSet rs = stmt.getResultSet();

        while (rs.next())
        {
            Schedule sch = new Schedule();

            sch.setIdDate(rs.getString("idDate"));
            sch.setDate(rs.getString("date"));
            sch.setHours(rs.getString("hours"));
            sch.setIdEmp(rs.getString("id_employee"));
            sch.setRealNEmployee(rs.getString("realNEmployee"));
            sch.setIdCus(rs.getString("id_customer"));
            sch.setRealNCustomer(rs.getString("realNCustomer"));
            sch.setIdAddr(rs.getString("id_cAddress"));
            sch.setRealAddr(rs.getString("realAddress"));

            scheduleList.add(sch);

            scheduleJList.setModel(getListFromSlist(scheduleList));

        }

    }

    public void updateEmp(Employee employee) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "UPDATE employee SET  cnp=?,eName=?,eAddress=?,ePhone=?,eEmail=? WHERE id= ?;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(6, employee.getId());
            pstmt.setString(1, cprTxtField.getText());
            pstmt.setString(2, empNameTxtField.getText());
            pstmt.setString(3, empAdrtxttField.getText());
            pstmt.setString(4, empPhnTxtField.getText());
            pstmt.setString(5, empEmailTxtField.getText());

            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("--------------End---------------");
    }

    public void updateCust(Customer customer) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "UPDATE customer SET cvr=?,cName=?,cPhone=?,cEmail=? WHERE cId=?;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(5, customer.getcId());
            pstmt.setString(1, cvrTxtField.getText());
            pstmt.setString(2, customerNameTField.getText());

            pstmt.setString(3, cPhoneTField.getText());
            pstmt.setString(4, cEmailTField.getText());

            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("--------------End---------------");
    }

    public void updateCAddress(Address address) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "UPDATE caddress SET address1=?,address2=?,address3=?,address4=? WHERE idCAddress=?;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(5, cIdAddressTxF.getText());

            pstmt.setString(1, cAddress1TField.getText());
            pstmt.setString(2, cAddress2TField.getText());
            pstmt.setString(3, cAddress3TField.getText());
            pstmt.setString(4, cAddress4TField.getText());

            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("--------------End---------------");
    }

    public void upDateSchedule(Schedule schedule) throws ClassNotFoundException
    {
        try
        {

            Connection conn = DBConnector.getConnection();

            String sql = "UPDATE date SET date=?,hours=?,id_Employee=?,realNEmployee=?,id_Customer=?,realNCustomer=?,id_cAddress=?,realAddress=? WHERE idDate=?;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            String employee = schEmpComb.getSelectedItem().toString();
            String[] partEmp = employee.split("-");
            String idEmp = partEmp[0];
            String realNEmp = partEmp[1];

            String customer = schCustComb.getSelectedItem().toString();

            String[] partCust = customer.split("-");
            String idCus = partCust[0];
            String realNCust = partCust[1];

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
            String date = dateFormat.format(jWorkDate.getDate());
            //String date = schdateTextField.getText();

            String location = schLocationComb.getSelectedItem().toString();
            String[] partLoc = location.split("-");
            String idLoc = partLoc[0];
            String realLoc = partLoc[1];

            pstmt.setString(1, date);
            pstmt.setString(2, schWrkHouTxtField.getText());
            pstmt.setString(3, idEmp);
            pstmt.setString(4, realNEmp);
            pstmt.setString(5, idCus);
            pstmt.setString(6, realNCust);
            pstmt.setString(7, idLoc);
            pstmt.setString(8, realLoc);
            pstmt.setString(9, idSch.getText());

            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

        System.out.println("------------End-----------");

    }

    private void readData()
    {
        if (ok != 0)
        {
            DefaultListModel listModel = (DefaultListModel) empJList.getModel();
            listModel.removeAllElements();
            DefaultListModel listMCustomer = (DefaultListModel) customerJList.getModel();
            listMCustomer.removeAllElements();
            DefaultListModel listMSchedule = (DefaultListModel) scheduleJList.getModel();
            listMSchedule.removeAllElements();
            DefaultListModel listMAddress = (DefaultListModel) addressJList.getModel();
            listMAddress.removeAllElements();
        }
        try
        {
            select();
            cSelect();
            sSelect();
            cAddresSelect();

        } catch (SQLException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }

        ok = 1;

    }

    public DefaultComboBoxModel buildComboBoxModel() throws ClassNotFoundException
    {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        String SQL = "SELECT * FROM employee;";

        try
        {

            Employee empl = new Employee();
            Connection conn = DBConnector.getConnection();
            PreparedStatement pstm = conn.prepareStatement(SQL);
            pstm.execute();
            ResultSet rs = pstm.executeQuery();
            while (rs.next())
            {
                String eId = rs.getString("id");
                String cnp = rs.getString("cnp");
                String eName = rs.getString("eName");
                String eAddress = rs.getString("eAddress");
                String ePhone = rs.getString("ePhone");
                String eEmail = rs.getString("eEmail");
                empl.setId(eId);
                empl.setCnp(cnp);
                empl.seteName(eName);
                empl.seteAddress(eAddress);
                empl.setePhone(ePhone);
                empl.seteEmail(eEmail);
                String totalE = empl.getId() + "-" + empl.geteName();
                schEmpComb.addItem(totalE);
            }
            rs.close();
            pstm.close();
        } catch (Exception e)
        {

        }
        return comboBoxModel;
    }

    Customer selectedCustomer = null;

    public DefaultComboBoxModel buildComboBoxModelCust() throws ClassNotFoundException
    {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        String SQL = "SELECT * FROM customer;";

        try
        {

            customerList.clear();

            Connection conn = DBConnector.getConnection();
            PreparedStatement pstm = conn.prepareStatement(SQL);
            pstm.execute();
            ResultSet rs = pstm.executeQuery();
            while (rs.next())
            {
                Customer c = new Customer();
                c.setCvr(rs.getString("Cvr"));
                c.setcEmail(rs.getString("cEmail"));
                c.setcId(rs.getString("cId"));
                c.setcName(rs.getString("cName"));
                c.setcPhone(rs.getString("cPhone"));
                c.setIdCustomer(rs.getInt("id_customer"));
                customerList.add(c);
                String totalC = c.getIdCustomer() + "-" + c.getcName();

                schCustComb.addItem(totalC);
            }

            rs.close();
            pstm.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return comboBoxModel;
    }

    public DefaultComboBoxModel buildComboBoxModelReportselectCust() throws ClassCastException
    {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        String SQL = "SELECT * FROM customer;";

        try
        {

            Connection conn = DBConnector.getConnection();
            PreparedStatement pstm = conn.prepareStatement(SQL);
            pstm.execute();
            ResultSet rs = pstm.executeQuery();
            while (rs.next())
            {
                Customer c = new Customer();

                c.setcName(rs.getString("cName"));

                customerList.add(c);
                String cust = c.getcName();

                resCusComboBox.addItem(cust);
            }

            rs.close();
            pstm.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return comboBoxModel;

    }

    public DefaultComboBoxModel buildComboBoxModelReportselectEmp() throws ClassNotFoundException
    {

        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();

        String SQL = "SELECT*FROM employee;";

        try
        {

            Connection conn = DBConnector.getConnection();
            PreparedStatement pstm = conn.prepareStatement(SQL);
            pstm.execute();
            ResultSet rs = pstm.executeQuery();

            while (rs.next())
            {

                Schedule e = new Schedule();

                e.setIdEmp(rs.getString("id"));
                e.setRealNEmployee(rs.getString("eName"));

                String display = e.getRealNEmployee();

                resEmpComboBox.addItem(display);

            }
            rs.close();
            pstm.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return comboBoxModel;
    }

   
   
    
    public static void exportList(ListModel model, File f) throws IOException {
    PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
    try {
        int len = model.getSize();
        for (int i = 0; i < len; i++) {
            pw.println(model.getElementAt(i).toString());
        }
    } finally {
        pw.close();
    }
}

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jTabbedPane3 = new javax.swing.JTabbedPane();
        jTabbedPane4 = new javax.swing.JTabbedPane();
        jMenu3 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenu7 = new javax.swing.JMenu();
        jMenu8 = new javax.swing.JMenu();
        ResultHours = new javax.swing.JTabbedPane();
        employeesTab = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        empJList = new javax.swing.JList();
        cprTxtField = new javax.swing.JTextField();
        empAdrtxttField = new javax.swing.JTextField();
        empNameTxtField = new javax.swing.JTextField();
        empPhnTxtField = new javax.swing.JTextField();
        cprLabel = new javax.swing.JLabel();
        empNameLabel = new javax.swing.JLabel();
        empAdrLabel = new javax.swing.JLabel();
        empPhnLabel = new javax.swing.JLabel();
        empUpdateButton = new javax.swing.JButton();
        empDelButton = new javax.swing.JButton();
        empAddButton = new javax.swing.JButton();
        empEmailTxtField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        empIdLabel = new javax.swing.JLabel();
        empIdTfield = new javax.swing.JTextField();
        scheduleTab = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        scheduleJList = new javax.swing.JList();
        schEmpComb = new javax.swing.JComboBox();
        schCustComb = new javax.swing.JComboBox();
        schWrkHouTxtField = new javax.swing.JTextField();
        empSchLabel = new javax.swing.JLabel();
        schCustLabel = new javax.swing.JLabel();
        shcWrkHouLabel = new javax.swing.JLabel();
        schConfButton = new javax.swing.JButton();
        schUpDateButt = new javax.swing.JButton();
        schDeleteButt = new javax.swing.JButton();
        dateEffective = new javax.swing.JLabel();
        schLocationComb = new javax.swing.JComboBox();
        schLoctLabel = new javax.swing.JLabel();
        idSch = new javax.swing.JTextField();
        jWorkDate = new com.toedter.calendar.JDateChooser();
        customerTab = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        customerJList = new javax.swing.JList();
        cvr = new javax.swing.JLabel();
        customerName = new javax.swing.JLabel();
        CaddressMLabel = new javax.swing.JLabel();
        cAddress1TField = new javax.swing.JTextField();
        phoneLabel = new javax.swing.JLabel();
        emailLabel = new javax.swing.JLabel();
        custAddButton = new javax.swing.JButton();
        updateCButton = new javax.swing.JButton();
        deleteCbutton = new javax.swing.JButton();
        cAddress3TField = new javax.swing.JTextField();
        cAddress2TField = new javax.swing.JTextField();
        cAddress4TField = new javax.swing.JTextField();
        Caddress2Label = new javax.swing.JLabel();
        Caddress3Label = new javax.swing.JLabel();
        Caddress4Label = new javax.swing.JLabel();
        cIdLabel = new javax.swing.JLabel();
        cvrTxtField = new javax.swing.JTextField();
        customerNameTField = new javax.swing.JTextField();
        cPhoneTField = new javax.swing.JTextField();
        cEmailTField = new javax.swing.JTextField();
        cIdTextField = new javax.swing.JTextField();
        cIdAddressTxF = new javax.swing.JTextField();
        jScrollPane5 = new javax.swing.JScrollPane();
        addressJList = new javax.swing.JList();
        updateCAddressButton = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        resultDisplayJList = new javax.swing.JList();
        displayButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        resEmpComboBox = new javax.swing.JComboBox();
        resCusComboBox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        resHtotalHoure = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jStartDate = new com.toedter.calendar.JDateChooser();
        jEndDate = new com.toedter.calendar.JDateChooser();
        writeToFile = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        jTabbedPane3.addTab("tab1", jTabbedPane4);

        jMenu3.setText("jMenu3");

        jMenu6.setText("jMenu6");

        jMenu7.setText("jMenu7");

        jMenu8.setText("jMenu8");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        empJList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        empJList.setToolTipText("");
        empJList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                empJListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(empJList);

        empAdrtxttField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                empAdrtxttFieldActionPerformed(evt);
            }
        });

        cprLabel.setText("CPR");

        empNameLabel.setText("Name");

        empAdrLabel.setText("Address");

        empPhnLabel.setText("Phone");

        empUpdateButton.setText("Update");
        empUpdateButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                empUpdateButtonActionPerformed(evt);
            }
        });

        empDelButton.setText("Delete");
        empDelButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                empDelButtonActionPerformed(evt);
            }
        });

        empAddButton.setText("Add");
        empAddButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                empAddButtonActionPerformed(evt);
            }
        });

        jLabel1.setText("Email");

        empIdLabel.setText("ID");

        empIdTfield.setText("1");
        empIdTfield.setEnabled(false);
        empIdTfield.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                empIdTfieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout employeesTabLayout = new javax.swing.GroupLayout(employeesTab);
        employeesTab.setLayout(employeesTabLayout);
        employeesTabLayout.setHorizontalGroup(
            employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeesTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(employeesTabLayout.createSequentialGroup()
                        .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(empAdrLabel)
                            .addComponent(jLabel1)
                            .addComponent(empPhnLabel)
                            .addComponent(empNameLabel)
                            .addComponent(cprLabel))
                        .addGap(25, 25, 25)
                        .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(empAddButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(empNameTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(cprTxtField)
                            .addComponent(empAdrtxttField)
                            .addComponent(empPhnTxtField, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                            .addComponent(empEmailTxtField))
                        .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(employeesTabLayout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(empIdLabel)
                                .addGap(18, 18, 18)
                                .addComponent(empIdTfield, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(employeesTabLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(empUpdateButton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(empDelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 365, Short.MAX_VALUE)))
                .addContainerGap())
        );
        employeesTabLayout.setVerticalGroup(
            employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeesTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cprTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cprLabel)
                    .addComponent(empIdLabel)
                    .addComponent(empIdTfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empNameLabel)
                    .addComponent(empNameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(empAdrtxttField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(empAdrLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empPhnTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(empPhnLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empEmailTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(employeesTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empAddButton)
                    .addComponent(empUpdateButton)
                    .addComponent(empDelButton))
                .addGap(0, 87, Short.MAX_VALUE))
        );

        ResultHours.addTab("Employees", employeesTab);

        scheduleJList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                scheduleJListValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(scheduleJList);

        schEmpComb.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                schEmpCombActionPerformed(evt);
            }
        });

        schCustComb.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                schCustCombItemStateChanged(evt);
            }
        });
        schCustComb.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                schCustCombActionPerformed(evt);
            }
        });

        empSchLabel.setText("Set Employe");

        schCustLabel.setText("Customer");

        shcWrkHouLabel.setText("Working Hours");

        schConfButton.setText("O.K");
        schConfButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                schConfButtonActionPerformed(evt);
            }
        });

        schUpDateButt.setText("Update");
        schUpDateButt.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                schUpDateButtActionPerformed(evt);
            }
        });

        schDeleteButt.setText("Delete");
        schDeleteButt.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                schDeleteButtActionPerformed(evt);
            }
        });

        dateEffective.setText("Date");

        schLocationComb.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                schLocationCombActionPerformed(evt);
            }
        });

        schLoctLabel.setText("Location");

        idSch.setEnabled(false);

        jWorkDate.setDateFormatString("yyyy-M-dd");

        javax.swing.GroupLayout scheduleTabLayout = new javax.swing.GroupLayout(scheduleTab);
        scheduleTab.setLayout(scheduleTabLayout);
        scheduleTabLayout.setHorizontalGroup(
            scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scheduleTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, scheduleTabLayout.createSequentialGroup()
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(schEmpComb, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(empSchLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(schCustLabel)
                                    .addComponent(schCustComb, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addComponent(shcWrkHouLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(schWrkHouTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(dateEffective)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jWorkDate, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)))
                        .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(schLocationComb, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(schConfButton, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 464, Short.MAX_VALUE)
                                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(schDeleteButt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(schUpDateButt, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(scheduleTabLayout.createSequentialGroup()
                                .addComponent(schLoctLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(idSch, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        scheduleTabLayout.setVerticalGroup(
            scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scheduleTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(empSchLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schCustLabel)
                    .addComponent(schLoctLabel)
                    .addComponent(idSch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(schLocationComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schCustComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schEmpComb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(schUpDateButt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jWorkDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(scheduleTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(shcWrkHouLabel)
                        .addComponent(schWrkHouTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(dateEffective)
                        .addComponent(schConfButton)
                        .addComponent(schDeleteButt)))
                .addGap(36, 36, 36))
        );

        ResultHours.addTab("Schedule", scheduleTab);

        customerJList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                customerJListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(customerJList);

        cvr.setText("CVR");

        customerName.setText("C.Name");

        CaddressMLabel.setText("Main Address");

        cAddress1TField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cAddress1TFieldActionPerformed(evt);
            }
        });

        phoneLabel.setText("Phone");

        emailLabel.setText("C.Email");

        custAddButton.setText("Add");
        custAddButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                custAddButtonActionPerformed(evt);
            }
        });

        updateCButton.setText("Update");
        updateCButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                updateCButtonActionPerformed(evt);
            }
        });

        deleteCbutton.setText("Delete");
        deleteCbutton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                deleteCbuttonActionPerformed(evt);
            }
        });

        cAddress4TField.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cAddress4TFieldActionPerformed(evt);
            }
        });

        Caddress2Label.setText("Address 2");

        Caddress3Label.setText("Address 3");

        Caddress4Label.setText("Address 4");

        cIdLabel.setText("C.Id");

        cIdTextField.setText("1");
        cIdTextField.setEnabled(false);

        addressJList.addListSelectionListener(new javax.swing.event.ListSelectionListener()
        {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt)
            {
                addressJListValueChanged(evt);
            }
        });
        jScrollPane5.setViewportView(addressJList);

        updateCAddressButton.setText("UpdateAddress");
        updateCAddressButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                updateCAddressButtonActionPerformed(evt);
            }
        });

        jLabel6.setText("Address List");

        javax.swing.GroupLayout customerTabLayout = new javax.swing.GroupLayout(customerTab);
        customerTab.setLayout(customerTabLayout);
        customerTabLayout.setHorizontalGroup(
            customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customerTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(customerTabLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 691, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(customerTabLayout.createSequentialGroup()
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(customerTabLayout.createSequentialGroup()
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(customerTabLayout.createSequentialGroup()
                                        .addComponent(cIdLabel)
                                        .addGap(43, 43, 43))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, customerTabLayout.createSequentialGroup()
                                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(emailLabel)
                                            .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(customerName)
                                                .addComponent(phoneLabel)
                                                .addComponent(cvr)))
                                        .addGap(18, 18, 18)))
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, customerTabLayout.createSequentialGroup()
                                        .addComponent(cIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(customerTabLayout.createSequentialGroup()
                                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(deleteCbutton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(updateCButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(cvrTxtField)
                                            .addComponent(customerNameTField, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cPhoneTField, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cEmailTField, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(custAddButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(167, 167, 167)
                                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(Caddress4Label)
                                            .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(CaddressMLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(Caddress2Label)
                                                .addComponent(Caddress3Label)))))
                                .addGap(28, 28, 28)
                                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(updateCAddressButton, javax.swing.GroupLayout.DEFAULT_SIZE, 282, Short.MAX_VALUE)
                                        .addComponent(cAddress4TField)
                                        .addComponent(cAddress3TField)
                                        .addComponent(cAddress1TField)
                                        .addComponent(cAddress2TField))
                                    .addGroup(customerTabLayout.createSequentialGroup()
                                        .addGap(288, 288, 288)
                                        .addComponent(cIdAddressTxF, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jScrollPane1))
                        .addContainerGap())))
        );
        customerTabLayout.setVerticalGroup(
            customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(customerTabLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(customerTabLayout.createSequentialGroup()
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cIdLabel)
                            .addComponent(cIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cvr)
                            .addComponent(cvrTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(customerNameTField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(customerName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cPhoneTField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(phoneLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(emailLabel)
                            .addComponent(cEmailTField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(custAddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateCButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deleteCbutton))
                    .addGroup(customerTabLayout.createSequentialGroup()
                        .addComponent(cIdAddressTxF, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cAddress1TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CaddressMLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Caddress2Label)
                            .addComponent(cAddress2TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cAddress3TField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Caddress3Label))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(customerTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cAddress4TField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Caddress4Label))
                        .addGap(18, 18, 18)
                        .addComponent(updateCAddressButton)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        ResultHours.addTab("Customers", customerTab);

        jScrollPane4.setViewportView(resultDisplayJList);

        displayButton.setText("Display");
        displayButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                displayButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Select Employee");

        jLabel3.setText("From Date");

        jLabel4.setText("To Date");

        resEmpComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "All" }));
        resEmpComboBox.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                resEmpComboBoxActionPerformed(evt);
            }
        });

        resCusComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "All" }));

        jLabel5.setText("Select Customer");

        jLabel7.setText("Total Working Hours");

        jStartDate.setDateFormatString("dd-M-yyyy");
        jStartDate.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mouseClicked(java.awt.event.MouseEvent evt)
            {
                jStartDateMouseClicked(evt);
            }
        });

        jEndDate.setDateFormatString("dd-M-yyyy");

        writeToFile.setText("GenerateReport");
        writeToFile.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                writeToFileActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane4)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 28, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(resCusComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(resEmpComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2)
                                .addGap(122, 122, 122)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jStartDate, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(333, 333, 333))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(54, 54, 54)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(writeToFile)
                                    .addComponent(displayButton))
                                .addGap(72, 72, 72))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(resHtotalHoure, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(237, 237, 237))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel3))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel5)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(resEmpComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(resCusComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(displayButton, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(jStartDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jEndDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(resHtotalHoure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addComponent(writeToFile))
                .addGap(42, 42, 42))
        );

        ResultHours.addTab("ResultHours", jPanel1);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(ResultHours, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ResultHours)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cAddress1TFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cAddress1TFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cAddress1TFieldActionPerformed

    private void empAdrtxttFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empAdrtxttFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_empAdrtxttFieldActionPerformed

    private void empUpdateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empUpdateButtonActionPerformed

        //int selectedIndex = empJList.getSelectedIndex();
        String id = empIdTfield.getText();
        String cnp = cprTxtField.getText();
        String eName = empNameTxtField.getText();
        String eAddress = empAdrtxttField.getText();
        String ePhone = empPhnTxtField.getText();
        String eEmail = empEmailTxtField.getText();

        Employee em = new Employee(id, cnp, eName, eAddress, ePhone, eEmail);// really need a constructor?

        try
        {
            updateEmp(em);

            employeeList.clear();
            empJList.setModel(getListFromAlist(employeeList));

            select();

            System.out.println("item updated");
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_empUpdateButtonActionPerformed

    private void empDelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empDelButtonActionPerformed

        int selectedIndex = empJList.getSelectedIndex();

        Employee empl = (Employee) empJList.getSelectedValue();

        if (empl != null)
        {
            employeeList.remove(selectedIndex);

            empJList.setModel(getListFromAlist(employeeList));

            try
            {
                crudE.deleteEmp(empl);

                eId = eId - 1;

            } catch (ClassNotFoundException ex)
            {
                java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }//GEN-LAST:event_empDelButtonActionPerformed

    private void empJListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_empJListValueChanged
        // TODO add your handling code here:

        Employee empl = (Employee) empJList.getSelectedValue();
        if (empl != null)
        {

            empIdTfield.setText(empl.getId());
            cprTxtField.setText(empl.getCnp());
            empNameTxtField.setText(empl.geteName());
            empAdrtxttField.setText(empl.geteAddress());
            empPhnTxtField.setText(empl.getePhone());
            empEmailTxtField.setText(empl.geteEmail());
        }


    }//GEN-LAST:event_empJListValueChanged

    private void empAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empAddButtonActionPerformed

        String id = empIdTfield.getText();
        String cnp = cprTxtField.getText();
        String eName = empNameTxtField.getText();
        String eAddress = empAdrtxttField.getText();
        String ePhone = empPhnTxtField.getText();
        String eEmail = empEmailTxtField.getText();

        Employee em = new Employee(id, cnp, eName, eAddress, ePhone, eEmail);// really need a constractor?

        employeeList.add(em);

        try
        {
            crudE.insertEmp(em);
            eId = eId + 1;

            empIdTfield.setText(eId + "");
            empJList.setModel(getListFromAlist(employeeList));
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_empAddButtonActionPerformed

    private void cAddress4TFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cAddress4TFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cAddress4TFieldActionPerformed

    private void empIdTfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empIdTfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_empIdTfieldActionPerformed

    private void custAddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_custAddButtonActionPerformed

        String cid = cIdTextField.getText();
        String idCAddress = cIdTextField.getText();

        String cvru = cvrTxtField.getText();
        String cName = customerNameTField.getText();
        String cAddress1 = cAddress1TField.getText();
        String cAddress2 = cAddress2TField.getText();
        String cAddress3 = cAddress3TField.getText();
        String cAddress4 = cAddress4TField.getText();
        String cPhone = cPhoneTField.getText();
        String cEmail = cEmailTField.getText();

        Customer cus = new Customer(cid, cvru, cName, cPhone, cEmail);
        Address addr = new Address(idCAddress, cAddress1, cAddress2, cAddress3, cAddress4);
        customerList.add(cus);

        try
        {
            crudC.insertCAddress(addr);
            crudC.insertCust(cus);

            cId = cId + 1;
            cIdTextField.setText(cId + "");
            customerList.clear();
            cSelect();

        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_custAddButtonActionPerformed

    private void deleteCbuttonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_deleteCbuttonActionPerformed
    {//GEN-HEADEREND:event_deleteCbuttonActionPerformed

        int selectedIndex = customerJList.getSelectedIndex();
        final JOptionPane optionPane = new JOptionPane(
                "The only way to close this dialog is by\n"
                + "pressing one of the following buttons.\n"
                + "Do you understand?",
                JOptionPane.QUESTION_MESSAGE,
                JOptionPane.YES_NO_OPTION);
        Customer custom = (Customer) customerJList.getSelectedValue();

        if (custom != null)
        {
            customerList.remove(selectedIndex);

            customerJList.setModel(getListFromlist(customerList));

            try
            {
                crudC.deleteCust(custom);

                cId = cId - 1;

            } catch (ClassNotFoundException ex)
            {
                java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_deleteCbuttonActionPerformed

    private void customerJListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_customerJListValueChanged
    {//GEN-HEADEREND:event_customerJListValueChanged

        Customer cust = (Customer) customerJList.getSelectedValue();

        String SQL = "SELECT * FROM customer c, caddress a WHERE a.idCAddress = c.id_customer AND c.id_customer = ?;";

        try
        {
            Connection conn = DBConnector.getConnection();
            PreparedStatement pstm = conn.prepareStatement(SQL);

            pstm.setString(1, cIdTextField.getText());

            ResultSet rs = pstm.executeQuery();

            while (rs.next())
            {

                Address addre = new Address();

                cIdAddressTxF.setText(rs.getString("idCAddress"));
                cAddress1TField.setText(rs.getString("address1"));
                cAddress2TField.setText(rs.getString("address2"));
                cAddress3TField.setText(rs.getString("address3"));
                cAddress4TField.setText(rs.getString("address4"));
                addre.setIdCAddress(rs.getString("idCAddress"));
                addre.setcAddress1(rs.getString("address1"));
                addre.setcAddress2(rs.getString("address2"));
                addre.setcAddress3(rs.getString("address3"));
                addre.setcAddress4(rs.getString("address4"));
                addressList.add(addre);

                addressJList.setModel(getListFromAlistDisplay(addressList));

            }

            rs.close();
            pstm.close();
            conn.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        if (cust != null)
        {

            cIdTextField.setText(cust.getcId());
            cvrTxtField.setText(cust.getCvr());
            customerNameTField.setText(cust.getcName());

            cPhoneTField.setText(cust.getcPhone());
            cEmailTField.setText(cust.getcEmail());

        }

    }//GEN-LAST:event_customerJListValueChanged

    private void updateCButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_updateCButtonActionPerformed
    {//GEN-HEADEREND:event_updateCButtonActionPerformed
        try
        {

            String cid = cIdTextField.getText();
            String cvru = cvrTxtField.getText();
            String cName = customerNameTField.getText();

            String cPhone = cPhoneTField.getText();
            String cEmail = cEmailTField.getText();

            Customer cus = new Customer(cid, cvru, cName, cPhone, cEmail);

            updateCust(cus);

            System.out.println("item updated");
            customerList.clear();
            customerJList.setModel(getListFromlist(customerList));
            cSelect();
            cIdTextField.setText("");
            cvrTxtField.setText("");
            customerNameTField.setText("");

            cPhoneTField.setText("");
            cEmailTField.setText("");

        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_updateCButtonActionPerformed

    private void displayButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_displayButtonActionPerformed
    {//GEN-HEADEREND:event_displayButtonActionPerformed


        
           Double i=0.0;
         
            Connection conn = DBConnector.getConnection();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");

            String start = dateFormat.format(jStartDate.getDate());
            String end = dateFormat.format(jEndDate.getDate());
        
         Object selected =resEmpComboBox.getSelectedItem();
         Object selectedCus = resCusComboBox.getSelectedItem();
         if(selected.toString().equals("All")&&selectedCus.toString().equals("All"))
       {
        try
        {
           
            
          
            
            
            
            String sqlRead = "SELECT * FROM date WHERE  date >='"+start+"' AND date<='"+end+"' GROUP BY date;";
            
            Statement  stmt = conn.createStatement();
            
            stmt.execute(sqlRead);
            ResultSet rs = stmt.getResultSet();
            
           
            
            while (rs.next())
            {
                Display_Schedule sch= new Display_Schedule();
                
                
              
                sch.setDate(rs.getString("date"));
                sch.setHours(rs.getString("hours"));
             
                sch.setRealNEmployee(rs.getString("realNEmployee"));
                sch.setRealNCustomer(rs.getString("realNCustomer"));
                
               
             
                sch.setRealAddr(rs.getString("realAddress"));
                
                displayList.add(sch);
               
                
                
                resultDisplayJList.setModel(getListFromSlistDisplay(displayList));
               
                i+=Double.valueOf(rs.getString("hours"));
                resHtotalHoure.setText(""+i);
              
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }

       } 
         
         else{
            try
        {
             
            String sqlRead = "SELECT * FROM date WHERE  date >='"+start+"' AND date<='"+end+"' GROUP BY date ;";
            
            Statement  stmt = conn.createStatement();
            
            stmt.execute(sqlRead);
            ResultSet rs = stmt.getResultSet();
            

            
            
              displayList.clear();
              resHtotalHoure.setText("");
            while (rs.next())
            {
                Display_Schedule sch= new Display_Schedule();
                
                
                sch.setDate(rs.getString("date"));
                sch.setHours(rs.getString("hours"));
             
                sch.setRealNEmployee(rs.getString("realNEmployee"));
                sch.setRealNCustomer(rs.getString("realNCustomer"));
                sch.setRealAddr(rs.getString("realAddress"));
               
               
                if(selected.toString().equals(rs.getString("realNEmployee"))&&(selectedCus.toString().equals("All")) )
                {   
                    
                    displayList.add(sch);
                    
                    i+=Double.valueOf(rs.getString("hours"));          
                    resHtotalHoure.setText(""+i);
                   
                }
               else if(selectedCus.toString().equals(rs.getString("realNCustomer"))&&(selected.toString().equals("All")))              
                {           
                            displayList.add(sch);
                            
                            i+=Double.valueOf(rs.getString("hours"));
                            resHtotalHoure.setText(""+i);
                 }
                else if(selectedCus.toString().equals(rs.getString("realNCustomer"))&&selected.toString().equals(rs.getString("realNEmployee")))              
                {            //displayList.clear();
                            displayList.add(sch);
                            
                            i+=Double.valueOf(rs.getString("hours"));
                            resHtotalHoure.setText(""+i);
                 }
                
                
                resultDisplayJList.setModel(getListFromSlistDisplay(displayList));
                
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }


        
         }  
        
    }//GEN-LAST:event_displayButtonActionPerformed

    private void resEmpComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resEmpComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_resEmpComboBoxActionPerformed

    private void addressJListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_addressJListValueChanged
    {//GEN-HEADEREND:event_addressJListValueChanged
        // TODO add your handling code here:
        Address addr = (Address) addressJList.getSelectedValue();

        if (addr != null)
        {

            cIdTextField.setText(addr.getIdCAddress());
            cAddress1TField.setText(addr.getcAddress1());
            cAddress2TField.setText(addr.getcAddress2());
            cAddress3TField.setText(addr.getcAddress3());
            cAddress4TField.setText(addr.getcAddress4());

        }

    }//GEN-LAST:event_addressJListValueChanged

    private void updateCAddressButtonActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_updateCAddressButtonActionPerformed
    {//GEN-HEADEREND:event_updateCAddressButtonActionPerformed

        String cAddressId = cIdAddressTxF.getText();
        String cAddress1 = cAddress1TField.getText();
        String cAddress2 = cAddress2TField.getText();
        String cAddress3 = cAddress3TField.getText();
        String cAddress4 = cAddress4TField.getText();
        Address addressNew = new Address(cAddressId, cAddress1, cAddress2, cAddress3, cAddress4);
        try
        {

            updateCAddress(addressNew);

            addressList.clear();
            addressJList.setModel(getListFromAlistDisplay(addressList));
            cAddresSelect();
            System.out.println("item updated");
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);

        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_updateCAddressButtonActionPerformed

    private void schLocationCombActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schLocationCombActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_schLocationCombActionPerformed

    private void schDeleteButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schDeleteButtActionPerformed
        // TODO add your handling code here:
        int selectedIndex = scheduleJList.getSelectedIndex();

        Schedule sched = (Schedule) scheduleJList.getSelectedValue();

        if (sched != null)
        {
            scheduleList.remove(selectedIndex);

            scheduleJList.setModel(getListFromSlist(scheduleList));

            try
            {
                crudS.deleteSch(sched);
                idDate = idDate - 1;

            } catch (ClassNotFoundException ex)
            {
                java.util.logging.Logger.getLogger(ConnectToMySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_schDeleteButtActionPerformed

    private void schConfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schConfButtonActionPerformed

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");

        String workDate = dateFormat.format(jWorkDate.getDate());
        //String end = dateFormat.format(jEndDate.getDate());
        String employee = schEmpComb.getSelectedItem().toString();
        String customer = schCustComb.getSelectedItem().toString();
        String location = schLocationComb.getSelectedItem().toString();
        String hours = schWrkHouTxtField.getText();
        String[] partEmp = employee.split("-");
        String idEmp = partEmp[0];
        String realNEmp = partEmp[1];
        //String date = schdateTextField.getText();
        String[] partCust = customer.split("-");
        String idCus = partCust[0];
        String realNCust = partCust[1];
        String[] partLoc = location.split("-");
        String idLoc = partLoc[0];
        String realLoc = partLoc[1];
        Schedule sch = new Schedule(hours, workDate, idEmp, realNEmp, idCus, realNCust, idLoc, realLoc);
        Schedule schDis = new Schedule(hours, workDate, realNEmp, realNCust, realLoc);

        scheduleList.add(schDis);

        crudS.inserSchedule(sch);

        scheduleList.clear();
        try
        {
            sSelect();
        } catch (SQLException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }


    }//GEN-LAST:event_schConfButtonActionPerformed

    private void schCustCombActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schCustCombActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_schCustCombActionPerformed

    private void schCustCombItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_schCustCombItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED)
        {
            Customer c = customerList.get(schCustComb.getSelectedIndex());
            schLocationComb.removeAllItems();

            String SQL = "SELECT * FROM customer c, caddress a WHERE a.idCAddress = c.id_customer AND c.id_customer = ?;";

            try
            {
                Connection conn = DBConnector.getConnection();
                PreparedStatement pstm = conn.prepareStatement(SQL);
                pstm.setInt(1, c.getIdCustomer());

                ResultSet rs = pstm.executeQuery();
                if (rs.next())
                {

                    Address addr = new Address();
                    addr.setIdCAddress(rs.getString("idCAddress"));
                    addr.setcAddress1(rs.getString("address1"));
                    addr.setcAddress2(rs.getString("address2"));
                    addr.setcAddress3(rs.getString("address3"));
                    addr.setcAddress4(rs.getString("address4"));

                    String totalCaddr1 = addr.getIdCAddress() + "-" + addr.getcAddress1();
                    schLocationComb.addItem(totalCaddr1);
                    String totalCaddr2 = addr.getIdCAddress() + "-" + addr.getcAddress2();
                    schLocationComb.addItem(totalCaddr2);
                    String totalCaddr3 = addr.getIdCAddress() + "-" + addr.getcAddress3();
                    schLocationComb.addItem(totalCaddr3);
                    String totalCaddr4 = addr.getIdCAddress() + "-" + addr.getcAddress4();
                    schLocationComb.addItem(totalCaddr4);

                } else
                {
                    System.out.println("NO DATA FOUND");
                }
                rs.close();
                pstm.close();
                conn.close();
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_schCustCombItemStateChanged

    private void schEmpCombActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schEmpCombActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_schEmpCombActionPerformed

    private void schUpDateButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_schUpDateButtActionPerformed
        try
        {

            String employee = schEmpComb.getSelectedItem().toString();
            String customer = schCustComb.getSelectedItem().toString();
            String location = schLocationComb.getSelectedItem().toString();
            String hours = schWrkHouTxtField.getText();
            String[] partEmp = employee.split("-");
            String idEmp = partEmp[0];
            String realNEmp = partEmp[1];
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-dd");
            String date = dateFormat.format(jWorkDate.getDate());
            //String date = schdateTextField.getText();
            String[] partCust = customer.split("-");
            String idCus = partCust[0];
            String realNCust = partCust[1];
            String[] partLoc = location.split("-");
            String idLoc = partLoc[0];
            String realLoc = partLoc[1];
            Schedule sch = new Schedule(hours, date, idEmp, realNEmp, idCus, realNCust, idLoc, realLoc);

            upDateSchedule(sch);

            System.out.println("item updated");
            scheduleList.clear();
            scheduleJList.setModel(getListFromSlist(scheduleList));

            sSelect();

        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_schUpDateButtActionPerformed

    private void scheduleJListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_scheduleJListValueChanged
    {//GEN-HEADEREND:event_scheduleJListValueChanged

        Schedule sch = (Schedule) scheduleJList.getSelectedValue();
        if (sch != null)
        {
            try
            {

                java.util.Date date = new SimpleDateFormat("yyyy-M-dd").parse(String.valueOf(sch.getDate()));
                jWorkDate.setDate(date);

                schWrkHouTxtField.setText(sch.getHours());

                idSch.setText(sch.getIdDate());
            } catch (ParseException ex)
            {
                Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
            }

        }


    }//GEN-LAST:event_scheduleJListValueChanged

    private void jStartDateMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_jStartDateMouseClicked
    {//GEN-HEADEREND:event_jStartDateMouseClicked

//        JXDatePicker picker;
//        picker = new JXDatePicker();
//        picker.setDate(Calendar.getInstance().getTime());
//        picker.setFormats(new SimpleDateFormat("dd.MM.yyyy"));
    }//GEN-LAST:event_jStartDateMouseClicked

    private void writeToFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_writeToFileActionPerformed
    {//GEN-HEADEREND:event_writeToFileActionPerformed
        DefaultListModel listModel = (DefaultListModel) resultDisplayJList.getModel();
        try
        {
          
            exportList(listModel, new File("report.txt"));
        } catch (IOException ex)
        {
            Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_writeToFileActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels())
            {
                if ("Nimbus".equals(info.getName()))
                {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                new Customer_Gui_V1().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Caddress2Label;
    private javax.swing.JLabel Caddress3Label;
    private javax.swing.JLabel Caddress4Label;
    private javax.swing.JLabel CaddressMLabel;
    private javax.swing.JTabbedPane ResultHours;
    private javax.swing.JList addressJList;
    private javax.swing.JTextField cAddress1TField;
    private javax.swing.JTextField cAddress2TField;
    private javax.swing.JTextField cAddress3TField;
    private javax.swing.JTextField cAddress4TField;
    private javax.swing.JTextField cEmailTField;
    private javax.swing.JTextField cIdAddressTxF;
    private javax.swing.JLabel cIdLabel;
    private javax.swing.JTextField cIdTextField;
    private javax.swing.JTextField cPhoneTField;
    private javax.swing.JLabel cprLabel;
    private javax.swing.JTextField cprTxtField;
    private javax.swing.JButton custAddButton;
    private javax.swing.JList customerJList;
    private javax.swing.JLabel customerName;
    private javax.swing.JTextField customerNameTField;
    private javax.swing.JPanel customerTab;
    private javax.swing.JLabel cvr;
    private javax.swing.JTextField cvrTxtField;
    private javax.swing.JLabel dateEffective;
    private javax.swing.JButton deleteCbutton;
    private javax.swing.JButton displayButton;
    private javax.swing.JLabel emailLabel;
    private javax.swing.JButton empAddButton;
    private javax.swing.JLabel empAdrLabel;
    private javax.swing.JTextField empAdrtxttField;
    private javax.swing.JButton empDelButton;
    private javax.swing.JTextField empEmailTxtField;
    private javax.swing.JLabel empIdLabel;
    private javax.swing.JTextField empIdTfield;
    private javax.swing.JList empJList;
    private javax.swing.JLabel empNameLabel;
    private javax.swing.JTextField empNameTxtField;
    private javax.swing.JLabel empPhnLabel;
    private javax.swing.JTextField empPhnTxtField;
    private javax.swing.JLabel empSchLabel;
    private javax.swing.JButton empUpdateButton;
    private javax.swing.JPanel employeesTab;
    private javax.swing.JTextField idSch;
    private com.toedter.calendar.JDateChooser jEndDate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private com.toedter.calendar.JDateChooser jStartDate;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTabbedPane jTabbedPane4;
    private com.toedter.calendar.JDateChooser jWorkDate;
    private javax.swing.JLabel phoneLabel;
    private javax.swing.JComboBox resCusComboBox;
    private javax.swing.JComboBox resEmpComboBox;
    private javax.swing.JTextField resHtotalHoure;
    private javax.swing.JList resultDisplayJList;
    private javax.swing.JButton schConfButton;
    private javax.swing.JComboBox schCustComb;
    private javax.swing.JLabel schCustLabel;
    private javax.swing.JButton schDeleteButt;
    private javax.swing.JComboBox schEmpComb;
    private javax.swing.JComboBox schLocationComb;
    private javax.swing.JLabel schLoctLabel;
    private javax.swing.JButton schUpDateButt;
    private javax.swing.JTextField schWrkHouTxtField;
    private javax.swing.JList scheduleJList;
    private javax.swing.JPanel scheduleTab;
    private javax.swing.JLabel shcWrkHouLabel;
    private javax.swing.JButton updateCAddressButton;
    private javax.swing.JButton updateCButton;
    private javax.swing.JButton writeToFile;
    // End of variables declaration//GEN-END:variables
}
