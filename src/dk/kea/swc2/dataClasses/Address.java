/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

/**
 *
 * @author yo
 */
public class Address
{

    private String idCAddress;
    private String cAddress1;
    private String cAddress2;
    private String cAddress3;
    private String cAddress4;

    public Address()
    {

    }

    public Address(String idCAddress, String cAddress1, String cAddress2, String cAddress3, String cAddress4)
    {
        this.idCAddress = idCAddress;
        this.cAddress1 = cAddress1;
        this.cAddress2 = cAddress2;
        this.cAddress3 = cAddress3;
        this.cAddress4 = cAddress4;
    }

    public Address(String cAddress1, String cAddress2, String cAddress3, String cAddress4)
    {
        this.cAddress1 = cAddress1;
        this.cAddress2 = cAddress2;
        this.cAddress3 = cAddress3;
        this.cAddress4 = cAddress4;
    }

    public String getIdCAddress()
    {
        return idCAddress;
    }

    public void setIdCAddress(String idCAddress)
    {
        this.idCAddress = idCAddress;
    }

    public String getcAddress1()
    {
        return cAddress1;
    }

    public void setcAddress1(String cAddress1)
    {
        this.cAddress1 = cAddress1;
    }

    public String getcAddress2()
    {
        return cAddress2;
    }

    public void setcAddress2(String cAddress2)
    {
        this.cAddress2 = cAddress2;
    }

    public String getcAddress3()
    {
        return cAddress3;
    }

    public void setcAddress3(String cAddress3)
    {
        this.cAddress3 = cAddress3;
    }

    public String getcAddress4()
    {
        return cAddress4;
    }

    public void setcAddress4(String cAddress4)
    {
        this.cAddress4 = cAddress4;
    }

    public String toString(){
        return cAddress1+ " "+cAddress2+ " "+cAddress3+ " "+cAddress4+ " ";
    }
}
