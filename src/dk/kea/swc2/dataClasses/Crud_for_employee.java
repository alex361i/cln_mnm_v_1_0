/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

import dk.kea.swc2.Gui.Customer_Gui_V1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author yo
 */
public class Crud_for_employee
{

    public void insertEmp(Employee employee) throws ClassNotFoundException
    {

        try
        {

            Connection conn = DBConnector.getConnection();

            String sql = "INSERT INTO employee (id,cnp,eName,eAddress,ePhone,eEmail) VALUES (?,?,?,?,?,?);";
            
            PreparedStatement pstmt = conn.prepareStatement(sql);
            //issue.getSubject();
            pstmt.setString(1, employee.getId());
            pstmt.setString(2, employee.getCnp());
            pstmt.setString(3, employee.geteName());
            pstmt.setString(4, employee.geteAddress());
            pstmt.setString(5, employee.getePhone());
            pstmt.setString(6, employee.geteEmail());
            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

        System.out.println("------------End-----------");

    }

    public static void deleteEmp(Employee employee) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "DELETE FROM employee WHERE id = ?;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, employee.getId());

            pstmt.execute();

        } catch (SQLException ex)
        {
            ex.printStackTrace();
        }

    }

}
