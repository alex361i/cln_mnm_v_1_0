/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

import dk.kea.swc2.Gui.Customer_Gui_V1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author yo
 */
public class Crud_for_schedule
{
    public void inserSchedule(Schedule schedule){
          try
        {

            Connection conn = DBConnector.getConnection();

            String sql = "INSERT INTO date (idDate,date,hours,id_Employee,realNEmployee,id_Customer,realNCustomer,id_cAddress,realAddress) VALUES (?,?,?,?,?,?,?,?,?);";

            PreparedStatement pstmt = conn.prepareStatement(sql);
            
            pstmt.setString (1, schedule.getIdDate());
            pstmt.setString (2, schedule.getDate());
            pstmt.setString (3, schedule.getHours());
            pstmt.setString (4, schedule.getIdEmp());
            pstmt.setString (5, schedule.getRealNEmployee());
            pstmt.setString (6, schedule.getIdCus());
            pstmt.setString (7, schedule.getRealNCustomer());
            pstmt.setString (8, schedule.getIdAddr());
            pstmt.setString (9, schedule.getRealAddr());
            pstmt.execute();
            //idDate++;
         
          
         

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

        System.out.println("------------End-----------");

    }
     public static void deleteSch(Schedule schedule) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "DELETE FROM date WHERE idDate = ?;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, schedule.getIdDate());

            pstmt.execute();

        } catch (SQLException ex)
        {

        }

    }
       
}