/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

import dk.kea.swc2.Gui.Customer_Gui_V1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author yo
 */
public class Crud_for_customer
{   
    
    public void insertCust(Customer customer) throws ClassNotFoundException
    {   
        

        try
        {

            Connection conn = DBConnector.getConnection();

            String sql = "INSERT INTO customer (cId,cvr,cName,cPhone,cEmail,id_customer) VALUES (?,?,?,?,?,?);";

            PreparedStatement pstmt = conn.prepareStatement(sql);
            //issue.getSubject();
            pstmt.setString(1, customer.getcId());
            pstmt.setString(2, customer.getCvr());
            pstmt.setString(3, customer.getcName());
            pstmt.setString(4, customer.getcPhone());
            pstmt.setString(5, customer.getcEmail());
            pstmt.setString(6, customer.getcId());
            
            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

        System.out.println("------------End-----------");

    }
    
    public void insertCAddress(Address address) throws ClassNotFoundException
    {

        try
        {

            Connection conn = DBConnector.getConnection();

            String sql = "INSERT INTO caddress (idCAddress,address1,address2,address3,address4) VALUES (?,?,?,?,?);";

            PreparedStatement pstmt = conn.prepareStatement(sql);
            //issue.getSubject();
            pstmt.setString(1, address.getIdCAddress());
            pstmt.setString(2, address.getcAddress1());
            pstmt.setString(3, address.getcAddress2());
            pstmt.setString(4, address.getcAddress3());
            pstmt.setString(5, address.getcAddress4());
           
            pstmt.execute();

        } catch (SQLException ex)
        {

            java.util.logging.Logger.getLogger(Customer_Gui_V1.class.getName()).log(Level.SEVERE, null, ex);

        }

        System.out.println("------------End-----------");

    }
     public static void deleteCust(Customer customer) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "DELETE customer,caddress  FROM caddress JOIN customer ON caddress.idCAddress=customer.id_customer WHERE customer.cId = ?  ;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, customer.getcId());

            pstmt.execute();

        } catch (SQLException ex)
        {
            System.out.println("Something is wrong"+customer);
            ex.printStackTrace();
        }

    }
     public static void deleteCAddress(Customer customer) throws ClassNotFoundException
    {

        try
        {
            Connection conn = DBConnector.getConnection();

            String sql = "DELETE FROM customer c,caddress a WHERE c.id_customer=a.idCAddress AND cId = ?  ;;";

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, customer.getcId());

            pstmt.execute();

        } catch (SQLException ex)
        {

        }

    }
     
}
