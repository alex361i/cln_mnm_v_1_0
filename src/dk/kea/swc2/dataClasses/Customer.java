/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

/**
 *
 * @author yo
 */
public class Customer
{

    private String cId;
    private String cvr;
    private String cName;
    private String cPhone;
    private String cEmail;
    private int idCustomer;


    public Customer()
    {
    }

    public Customer(String cId, String cvr, String cName, String cPhone, String cEmail)
    {
        this.cId = cId;
        this.cvr = cvr;
        this.cName = cName;
        this.cPhone = cPhone;
        this.cEmail = cEmail;
        
        
    }

    
    public String getcId()
    {
        return cId;
    }

    public void setcId(String cId)
    {
        this.cId = cId;
    }

    public String getCvr()
    {
        return cvr;
    }

    public void setCvr(String cvr)
    {
        this.cvr = cvr;
    }

    public String getcName()
    {
        return cName;
    }

    public void setcName(String cName)
    {
        this.cName = cName;
    }

   

    public String getcPhone()
    {
        return cPhone;
    }

    public void setcPhone(String cPhone)
    {
        this.cPhone = cPhone;
    }

    public String getcEmail()
    {
        return cEmail;
    }

    public void setcEmail(String cEmail)
    {
        this.cEmail = cEmail;
    }

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }
   
    

    public String toString()
    {
        return cId + " " + cvr + " " + cName + " " + cPhone + " " + cEmail;
    }
}
