/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.dataClasses;

import javax.swing.JTextField;

/**
 *
 * @author yo
 */
public class Schedule {
    private String hours;
    private String date;
    private String idDate;
    private String idEmp;
    private String idCus;
    private String idAddr;
    private String realAddr;
    private String realNCustomer;
    private String realNEmployee;

    public Schedule(String hours, String date,  String idEmp, String realNEmployee, String idCus,String realNCustomer, String idAddr, String realAddr)
    {
        this.hours = hours;
        this.date = date;
        
        this.idEmp = idEmp;
        this.idCus = idCus;
        this.idAddr = idAddr;
        this.realAddr = realAddr;
        this.realNCustomer = realNCustomer;
        this.realNEmployee = realNEmployee;
    }

    public String getRealNCustomer()
    {
        return realNCustomer;
    }

    public void setRealNCustomer(String realNCustomer)
    {
        this.realNCustomer = realNCustomer;
    }

    public String getRealNEmployee()
    {
        return realNEmployee;
    }

    public void setRealNEmployee(String realNEmployee)
    {
        this.realNEmployee = realNEmployee;
    }
   
   

    public Schedule() {
    }

    public Schedule(String hours, String date) {
        this.hours = hours;
        this.date = date;
        
    }
    public Schedule(String hours, String date,String idEmp,String idCus, String idAddr,String realAddr){
        this.hours = hours;
        this.date = date;
        this.idEmp =idEmp;
        this.idCus =idCus;
        this.idAddr=idAddr;
        this.realAddr=realAddr;
        
    }

    public Schedule(String hours, String date, String idEmp, String idCus, String idAddr)
    {
        this.hours = hours;
        this.date = date;
        
        this.idEmp = idEmp;
        this.idCus = idCus;
        this.idAddr = idAddr;
    }

    public String getRealAddr()
    {
        return realAddr;
    }

    public void setRealAddr(String realAddr)
    {
        this.realAddr = realAddr;
    }
   
   

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdDate() {
        return idDate;
    }

    public void setIdDate(String idDate) {
        this.idDate = idDate;
    }

    public String getIdEmp()
    {
        return idEmp;
    }

    public void setIdEmp(String idEmp)
    {
        this.idEmp = idEmp;
    }

    public String getIdCus()
    {
        return idCus;
    }

    public void setIdCus(String idCus)
    {
        this.idCus = idCus;
    }

    public String getIdAddr()
    {
        return idAddr;
    }

    public void setIdAddr(String idAddr)
    {
        this.idAddr = idAddr;
    }

    @Override
    public String toString()
    {
         return  "No of hours: " + hours + " Date: " + date + " " + idDate + " " + idEmp + " " + idCus + " " + idAddr + "Address: " + realAddr + " , " + realNCustomer + "   Employee  : " + realNEmployee ;
    }
    
   
    
    
   
   
    
}
